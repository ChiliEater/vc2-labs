#import "@preview/codelst:2.0.1": sourcecode
#set page(
  paper: "a4",
  numbering: "1 / 1"
)

#set text(
  font: "IBM Plex Sans",
)

#set heading(
  numbering: "1.",
)

#align(center, text(24pt)[
  *Report*
])

#align(center, text(10pt)[
  Jonas Costa, 29.04.2024 \
  #link("https://codeberg.org/ChiliEater/vc2-labs/src/branch/main/tracker-lab-04")
])

= Steps

First, we take some pictures:

#figure(
  image("mc_l.png")
)
#figure(
  image("mc_r.png")
)

The second image is slightly shifted to the right to simulate a stereo camera.

We then set the block size in our script to 21, the default value from the documentation. This should result in minimal noise. Resulting image kind of shows the original scene:

#figure(
  image("Figure_1.png")
)

This step also results in a point cloud file, which we can view in Blender.

= Results

As you may have guessed, the result isn't that great. The original image has many similar colors and surfaces. Some prominent features such as the center tree are clearly visible, though.

== Blender

I also thought of importing the point cloud into blender to see what it looks like.

#figure(
  image("img/points.png", width: 50%)
)

This doesn't look useful right now. It only needs a little bit of perspective shifting to look recognizable:

#figure(
  image("img/points_correct.png")
)

That's almost certainly the center tree again.

I also tried to make vertex colors work here but Blender just wouldn't play nice with me.

= Insights

Key takeaways: 

- This method of interpreting depth really struggles with similar colors
- Edges are kind of hard to determine since they're different for each eye
- Resolution doesn't seem to affect much
- Higher block size helps with busier images