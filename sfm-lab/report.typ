#import "@preview/codelst:2.0.1": sourcecode
#set page(
  paper: "a4",
  numbering: "1 / 1"
)

#set text(
  font: "IBM Plex Sans",
)

#set heading(
  numbering: "1.",
)

#align(center, text(24pt)[
  *Report*
])

#align(center, text(10pt)[
  Jonas Costa, 13.05.2024
])

= Steps

Regard3D, the recommended tool, is barely able to run let alone able to perform SfM and reconstruct a mesh. Particluarly on Linux it really isn't stable. I chose COLMAP instead to perform SfM as the first Gaussian Splatting paper makes use of it for that step.

First, we prepare some images. In my case, filmed my mouse from all angles and used ffmpeg to split into a few images. It's important to not input too many images as we'll run out of memory otherwise.

#grid(
  columns: (auto, auto, auto),
  gutter: 5pt,
  ..(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14).map(nr => [
      #figure(
        image("img/mouse/" + str(nr) + ".png"), caption: "Source Image #" + str(nr)
      )#label("frame-" + str(nr))
    ])
)

We then setup our COLMAP project by specifiying a few paths and some preset values.

#figure(
  image("img/colmap_1.png")
)

In the resulting point cloud, we can see each camera angle and position that was recognised.

#figure(
  image("img/colmap_2.png")
)

Then, we can load the actual dense cloud that was generated.

#figure(
  image("img/colmap_3.png")
)

Here we can actually see some structure of the mouse that was preserved. The mousepad was preserved in very high detail, surprisingly. We can now load the cloud into meshlab to perform mesh reconstruction.

#figure(
  image("img/colmap_4.png")
)

Unfortunately, it looks like our input doesn't provide sufficient information for the reconstruction to produce good output. Zooming in gives us some insight.

#figure(
  image("img/colmap_5.png")
)

We can see that the meshing algorithm was very successfull on the mouse pad, but less so on the mouse itself. There are some tris there but not really a signifcant amount.