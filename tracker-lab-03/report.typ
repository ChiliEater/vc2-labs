#import "@preview/codelst:2.0.1": sourcecode
#set page(
  paper: "a4",
  numbering: "1 / 1"
)

#set text(
  font: "IBM Plex Sans",
)

#set heading(
  numbering: "1.",
)

#align(center, text(24pt)[
  *Report*
])

#align(center, text(10pt)[
  Jonas Costa, 22.04.2024 \
  #link("https://codeberg.org/ChiliEater/vc2-labs/src/branch/main/tracker-lab-03")
])

= Prep Work

First, we need to fix the script as it is currently broken. Contrary to the heading comment in the file, the SIFT algorithm's patent has expired in 2023 and is thus once again available in the OpenCV repos. If we apply the following patch and install numpy, matplotlib, pyqt6, opencv-python, opencv-contrib-python we'll get a working script.

#sourcecode[```diff
diff --git a/tracker-lab-03/SIFT_demo.py b/tracker-lab-03/SIFT_demo.py
index 9cd46b5..c54b642 100644
--- a/tracker-lab-03/SIFT_demo.py
+++ b/tracker-lab-03/SIFT_demo.py
@@ -11,7 +11,7 @@ img1 = cv.imread('scene.pgm',cv.IMREAD_GRAYSCALE)   # queryImage
 img2 = cv.imread('basmati.pgm',cv.IMREAD_GRAYSCALE) # trainImage
 
 # Initiate SIFT detector
-sift = cv.xfeatures2d.SIFT_create()
+sift = cv.SIFT_create()
 # find the keypoints and descriptors with SIFT
 kp1, des1 = sift.detectAndCompute(img1,None)
 kp2, des2 = sift.detectAndCompute(img2,None)
@@ -39,4 +39,5 @@ for m,n in matches:
 imgMatches = cv.drawMatchesKnn(img1, kp1, img2, kp2, good, None, flags=cv.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)
 plt.figure("Matches")
 plt.axis('off')
-plt.imshow(imgMatches),plt.show()
\ No newline at end of file
+plt.imshow(imgMatches)
+plt.show()
\ No newline at end of file
```]

= Results

The script reads to different images and computes SIFT keypoints and descriptors. Then, it displays those keypoints. Lastly, matching keypoints are found between the two images and shown to the user. Poor results are filtered out.

#figure(
  image("img/comparison.png")
)

== Custom images

Using a bottle's logo and a zoomed out view of the scene, we can try out this algorithm with our own data.

#figure(
  image("img/bottle.jpg", width: 100%)
)

#figure(
  image("img/scene_bottle.jpg", width: 100%)
)

While there is a lot of noise, reducing the ratio threshold to 0.65 yields a result that clearly marks the both instances of the logo.

#figure(
  image("img/custom.png", width: 100%)
)

= Insights

From our experiments we can derive that non-grayscale images don't jive well with SIFT. That would explain why the existing images were in the PGM format. Furthermore, adjusting the threshold can be helpful when using sub-optimal images to clean up much of the noisy results. Speaking of sub-optimal, curved or angled source images can be extra challenging for SIFT so avoiding that is helpful.