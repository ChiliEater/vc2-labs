in vec2 uvVS;
uniform sampler2D textureFS;

void main(void)
{
  gl_FragColor = texture2D(textureFS, uvVS);
}