uniform float u_mix_ratio;
uniform samplerCube u_cubemap;
uniform vec3 u_base_color;

in vec3 vs_reflection;
in vec3 vs_light_direction;
in float vs_lambert;

void main(void)
{
  vec4 reflection_color = textureCube(u_cubemap, vs_reflection);
  vec3 diffuse = u_base_color * vs_lambert;
  vec3 color = mix(reflection_color.xyz, diffuse, u_mix_ratio);
  gl_FragColor = vec4(color,1.0);
  //gl_FragColor = vec4(reflection_color.xyz,1.0);
}