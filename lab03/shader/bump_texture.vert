out vec2 uvVS;

void main(void)
{
  uvVS = uv;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}