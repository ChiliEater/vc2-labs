out vec3 vs_reflection;
out float vs_lambert;
out vec3 vs_light_direction;
uniform vec3 u_light_position;

void main(void)
{
  vs_light_direction = normalize(u_light_position - position);
  vs_reflection = reflect(normalize(position - cameraPosition), normalize(normal));
  vs_lambert = max(0.0, dot(u_light_position, normalize(normal)));
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}