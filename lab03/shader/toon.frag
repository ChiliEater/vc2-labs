in vec3 normalVS;
uniform vec3 lightDirection;
uniform mat4 colorBands;
const vec4 colorSteps = vec4(0.0, 0.3, 0.6, 0.8);

void main(void)
{
  float lightAngle = dot(lightDirection, normalVS);

  if (lightAngle > colorSteps.w) {
    gl_FragColor = colorBands[3].xyzw;
  } else if (lightAngle > colorSteps.z) {
    gl_FragColor = colorBands[2].xyzw;
  } else if (lightAngle > colorSteps.y) {
    gl_FragColor = colorBands[1].xyzw;
  } else {
    gl_FragColor = colorBands[0].xyzw;
  }
}