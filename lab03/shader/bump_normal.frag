uniform sampler2D u_texture;
uniform sampler2D u_normal_map;
uniform vec3 u_ambient_color;
uniform vec3 u_diffuse_color;
uniform vec3 u_specular_color;
uniform float u_shininess;
in vec3 i_light_direction;
in vec3 i_camera_direction;
in vec2 u_uv;

void main(void)
{
  vec3 normal = texture2D(u_normal_map, u_uv).xyz * 2.0 - 1.0;
  vec3 light_direction = normalize(i_light_direction);
  vec3 camera_direction = normalize(i_camera_direction);

  vec3 half_vector = normalize(light_direction + camera_direction);
  float lambert = max(0.0, dot(normal, light_direction));
  float phong = max(0.0, dot(normal, half_vector));
  float specular_power = pow(phong, u_shininess);
  vec3 diffuse = u_diffuse_color * lambert;
  vec3 specular = u_specular_color * specular_power;

  gl_FragColor = vec4(u_ambient_color * texture2D(u_texture, u_uv).xyz + diffuse * texture2D(u_texture, u_uv).xyz + specular, 1.0);
}