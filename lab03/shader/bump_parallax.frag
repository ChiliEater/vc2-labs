uniform sampler2D u_texture;
uniform sampler2D u_normal_map;
uniform vec3 u_ambient_color;
uniform vec3 u_diffuse_color;
uniform vec3 u_specular_color;
uniform float u_shininess;
in vec3 i_light_direction;
in vec3 i_camera_direction;
in vec2 u_uv;
uniform sampler2D u_height_map;
uniform float u_scale;
uniform float u_bias;

void main(void)
{
  vec3 light_direction = normalize(i_light_direction);
  vec3 camera_direction = normalize(i_camera_direction);

  float height = texture2D(u_height_map, u_uv).r;
  float hsb = height * u_scale + u_bias;
  vec2 parallax_uv = u_uv + hsb * camera_direction.xy;

  vec3 normal = texture2D(u_normal_map, parallax_uv).xyz * 2.0 - 1.0;
  vec3 half_vector = normalize(light_direction + camera_direction);
  float lambert = max(0.0, dot(normal, light_direction));
  float phong = max(0.0, dot(normal, half_vector));
  float specular_power = pow(phong, u_shininess);
  vec3 diffuse = u_diffuse_color * lambert;
  vec3 specular = u_specular_color * specular_power;

  gl_FragColor = vec4(u_ambient_color * texture2D(u_texture, parallax_uv).xyz + diffuse * texture2D(u_texture, parallax_uv).xyz + specular, 1.0);
}