uniform vec3 u_light_position;
uniform vec3 u_light_tangent;

out vec3 i_light_direction;
out vec3 i_camera_direction;
out vec2 u_uv;

void main(void)
{
  vec3 bitangent = cross(normal, normalize(u_light_tangent));
  
  mat3 TBN = mat3(normalize(u_light_tangent), bitangent, normal);
  i_light_direction = TBN * normalize(u_light_position - position);
  i_camera_direction = TBN * normalize(cameraPosition - position);
  u_uv = uv;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}