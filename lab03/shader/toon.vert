out vec3 normalVS;

void main(void)
{
  normalVS = normalMatrix * normal;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}