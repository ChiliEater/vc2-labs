import os
from mediapipe import solutions
from mediapipe.framework.formats import landmark_pb2
import mediapipe as mp
from mediapipe.tasks import python
from mediapipe.tasks.python import vision
import numpy as np
import cv2
import sys
import glob

save_dir = "./export"
import_dir = "./frames"
files = glob.glob("./frames/*")
print(f"the following files will be analysed: {files}")

def cv2_imshow(title, img):
  cv2.imwrite(save_dir + "/" + title, img)
  #while 1:
    #cv2.imshow("result", img)
    #if cv2.waitKey() == 27:
    #  break


def draw_landmarks_on_image(rgb_image, detection_result):
  pose_landmarks_list = detection_result.pose_landmarks
  annotated_image = np.copy(rgb_image)

  # Loop through the detected poses to visualize.
  for idx in range(len(pose_landmarks_list)):
    pose_landmarks = pose_landmarks_list[idx]

    # Draw the pose landmarks.
    pose_landmarks_proto = landmark_pb2.NormalizedLandmarkList()
    pose_landmarks_proto.landmark.extend([
      landmark_pb2.NormalizedLandmark(x=landmark.x, y=landmark.y, z=landmark.z) for landmark in pose_landmarks
    ])
    solutions.drawing_utils.draw_landmarks(
      annotated_image,
      pose_landmarks_proto,
      solutions.pose.POSE_CONNECTIONS,
      solutions.drawing_styles.get_default_pose_landmarks_style())
  return annotated_image

for path in files:
  print(f"Analyse {path}")
  base_options = python.BaseOptions(model_asset_path='pose_landmarker.task')
  options = vision.PoseLandmarkerOptions(
      base_options=base_options,
      output_segmentation_masks=True)
  detector = vision.PoseLandmarker.create_from_options(options)

  filename = os.path.splitext(os.path.basename(path))[0]

  image = mp.Image.create_from_file(path)
  detection_result = detector.detect(image)
  annotated_image = draw_landmarks_on_image(image.numpy_view(), detection_result)
  cv2_imshow(filename + "_pose.jpeg", cv2.cvtColor(annotated_image, cv2.COLOR_RGB2BGR))

  segmentation_mask = detection_result.segmentation_masks[0].numpy_view()
  visualized_mask = np.repeat(segmentation_mask[:, :, np.newaxis], 3, axis=2) * 255
  cv2_imshow(filename + "_seg.jpeg", visualized_mask)

