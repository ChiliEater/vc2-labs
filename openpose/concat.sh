#!/bin/bash
ffmpeg -f image2 -framerate 15 -i export/out%d_pose.jpeg -vcodec libx264 -crf 22 pose.mp4
ffmpeg -f image2 -framerate 15 -i export/out%d_seg.jpeg -vcodec libx264 -crf 22 seg.mp4