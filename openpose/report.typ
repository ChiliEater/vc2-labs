#import "@preview/codelst:2.0.1": sourcecode
#set page(
  paper: "a4",
  numbering: "1 / 1"
)

#set text(
  font: "IBM Plex Sans",
)

#set heading(
  numbering: "1.",
)

#align(center, text(24pt)[
  *Report*
])

#align(center, text(10pt)[
  Jonas Costa, 13.05.2024
])

= Steps

For this lab, we'll be using MediaPipe as it is relatively modern and panless to setup. First, we'll setup a pipenv:

#sourcecode[```
pipenv install mediapipe
```]

Then, we copy the source code from the MediaPipe demo notebook. We also download the model used in the demo:

#sourcecode[```sh
wget -O pose_landmarker.task -q 'https://storage.googleapis.com/mediapipe-models/pose_landmarker/pose_landmarker_heavy/float16/1/pose_landmarker_heavy.task'
```]

#sourcecode[#raw(read("landmarker.py"), lang: "py")]

We also create a simple debug config to run our script:

#sourcecode[#raw(read(".vscode/launch.json"), lang: "json")]

Now we just need to grab a test image:

#sourcecode[```sh
wget -O mountain-1080.jpg -q 'https://unsplash.com/ph
otos/827XUhVSp8M/download?ixid=M3wxMjA3fDB8MXxzZ
WFyY2h8MTZ8fHBlcnNvbnxlbnwwfHx8fDE3MTU1OTEzMTh8M
g&force=true&w=1920'
```]

When running the script, we get a great result.

#figure(
  image("img/result_0.png")
)

= Taking It Further

Next, we tried to make a video of the tracking. First we recorded a video at 1080p60.

#figure(
  image("img/sebi.png")
)

We then sperated the video using ffmpeg:

#sourcecode[```sh
ffmpeg -i vid.mp4 -vf fps=15 frames/out%d.png
```]

The script also needs to be modified:

#sourcecode[#raw(read("diff.patch"), lang: "diff")]

Now we concatenate the resulting images into new videos:

#sourcecode[#raw(read("concat.sh"), lang: "bash")]

The results are also very good!

#figure(
  image("img/sebi-track.jpeg")
)

#figure(
  image("img/sebi-seg.jpeg")
)