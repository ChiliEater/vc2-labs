#set page(
  paper: "a4",
  numbering: "1 / 1"
)

#set text(
  font: "IBM Plex Sans",
)

#set heading(
  numbering: "1.",
)

#align(center, text(24pt)[
  *Report*
])

#align(center, text(10pt)[
  Jonas Costa, 15.04.2024
])

= Printing

We first print out our checkerboard pattern and take some images of it from a few different angles. There isn't much to say about this.

#figure(
  image("img/01_pic.png", width: 80%)
)

= Calibrating

In the next step, we generate the calibration data using the images that we took before. The amount of pictures fed into the calibration doesn't seem to affect the stability of the tracking after 3 images. The figure below showcases one of these calibration steps.

#figure(
  image("img/02_markers.png", width: 80%)
)

This works by using OpenCV's `findChessboardCorners` to determine all the internal corners of the checkerboard. While it works decently well, it seems to not be super accurate all the time. It's enough for tracking these simple markers, though.

At the end of the routine, the gathered data points are passed to the `calibrateCamera` function which returns a bunch matrices and values.

#figure($
  "cameraMatrix" &= mat(
  455.03447746, 0, 316.61486619;
  0, 430.71746649, 280.56285266;;
  0, 0, 1
  ) \
  "distCoeff" &= mat(1.12986178, -6.24083093, 0.14870356, 0.04930866, 9.63914886)
$
)

The above data is serialized into a YAML-file for later use.

= Tracking

Lastly, we feed a video stream into the program to use the calibration data to detect and read ArUco marker data.

#figure(
  image("img/03_tracking.png", width: 80%)
)

The program processes each video frame using the `detectMarkers` function. If IDs are detected, estimate the rotation and translation relative to the camera. Then, draw a border around each marker and place an axis gizmo to show the normals.

This works very reliably up until ID 249, after which the program is unable to recognise any other markers. Viewing the markers at very steep angles also leads to unstable results. The algorithm seems pretty resistant to calibration errors from the previous step.


